# keras口罩检测

#### 介绍
使用keras搭建fasterRCNN，在VOC格式的口罩数据集上训练，达到了检测人群中有无戴口罩的目的

#### 软件架构
详细理论与描述见我的博客：
https://blog.csdn.net/qq_40943760/article/details/106354246


#### 安装教程

1.  需要numpy,matplotlib,scikit-learn,Pillow,tensorflow1.x,keras
2.  pip install package或者conda install package
3.  如果要训练，需要使用VOC格式数据集，需要安装labelimg

#### 使用说明

1.  ./model_data/logs下必须有模型权重文件，由于权重较大，所以未上传
2.  ./theory下是做这个东西的流程与方便理解代码的原理说明
3.  ./net下是fasterRCNN的模型构建
4.  run.py用于直接运行看结果，voctrain.py用于训练自己的VOC数据集

#### 结果显示
![输入图片说明](https://images.gitee.com/uploads/images/2020/0526/150645_47127a36_5644878.png "board.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0526/150708_aad4b88d_5644878.png "total.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0526/150718_cdcfb22f_5644878.png "acc.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0526/150729_72678ebb_5644878.png "结果.png")


